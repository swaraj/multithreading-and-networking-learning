package com.swaraj.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created with IntelliJ IDEA.
 * User: swaraj
 * Date: 3/11/13
 * Time: 9:27 AM
 */
public class ClientThread extends Thread {
    private ArrayBlockingQueue<Runnable> taskQueue = new ArrayBlockingQueue<Runnable>(3);
    int count = 0;

    public boolean addTask(Runnable r){
        if(count == 3){
            return false;
        }
        taskQueue.add(r);
        count++;
        return true;
    }

    @Override
    public void run() {
        while(true){
            try {
                Runnable r = taskQueue.take();
                if(r!=null){
                    count--;
                    r.run();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
