package com.swaraj.client;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

/**
 * Created with IntelliJ IDEA.
 * User: swaraj
 * Date: 3/10/13
 * Time: 9:42 PM
 */
public class Client {
    public static void main(String[] args){
        ClientThreadPool clientThreadPool = new ClientThreadPool(10);
        clientThreadPool.initialize();
        int i=0;
        while(true){
            System.out.println("iteration "+i);
            ClientTask clientTask = new ClientTask("localhost",8080);
            boolean result = clientThreadPool.execute(clientTask);
            if(!result){
                break;
            }
            i++;
        }
    }
}
