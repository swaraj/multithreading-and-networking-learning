package com.swaraj.client;

import java.util.List;
import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * User: swaraj
 * Date: 3/11/13
 * Time: 9:22 AM
 */
public class ClientThreadPool {
    private int size;
    private ClientThread[] threadPool = null;

    public ClientThreadPool(int size) {
        this.size = size;
        threadPool = new ClientThread[size];
    }

    public int getSize() {
        return size;
    }

    public void initialize(){
        if(threadPool==null) return;
        for(int i=0;i<size;i++){
            ClientThread clientThread = new ClientThread();
            clientThread.start();
            threadPool[i] = clientThread;
        }
    }

    public boolean execute(Runnable r){
        boolean served = false;
        int serveIndex = -1;
        for(int i=0;i<size;i++){
            if(threadPool[i].addTask(r)){
                served = true;
                serveIndex = i;
                break;
            }
        }
        if(served){
            System.out.println("Thread "+serveIndex+" is serving your task");
        }else{
            System.out.println("All Threads are busy");
        }

        return served;
    }
}
