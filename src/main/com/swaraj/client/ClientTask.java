package com.swaraj.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

/**
 * Created with IntelliJ IDEA.
 * User: swaraj
 * Date: 3/13/13
 * Time: 9:53 PM
 */
public class ClientTask implements Runnable {
    private String host;
    private int port;

    public ClientTask(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public void run() {
        PrintWriter printWriter = null;
        BufferedReader bufferedReader = null;
        Socket socket = null;
        try {
            socket = new Socket();
            SocketAddress socketAddress = new InetSocketAddress(host, port);
            socket.connect(socketAddress);
            printWriter = new PrintWriter(socket.getOutputStream());
            printWriter.println("hello server ");
            printWriter.flush();
            bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            System.out.println("from the server : " + bufferedReader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (printWriter != null) {
                printWriter.close();
            }
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (socket != null) {
                    socket.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
