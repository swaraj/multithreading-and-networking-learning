package com.swaraj.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
* Created with IntelliJ IDEA.
* User: swaraj
* Date: 3/11/13
* Time: 9:17 AM
* To change this template use File | Settings | File Templates.
*/
class ServerThread extends Thread {
    private ServerSocket serverSocket;

    public void setServerSocket(ServerSocket serverSocket) {
        this.serverSocket = serverSocket;
    }

    @Override
    public void run() {
        while(true){
            BufferedReader bufferedReader = null;
            PrintWriter printWriter = null;
            Socket socket = null;
            try {
                socket = serverSocket.accept();
                bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String line = bufferedReader.readLine();
                System.out.println("from the client : "+line);
                printWriter = new PrintWriter(socket.getOutputStream());
                printWriter.println("hello client");
                printWriter.flush();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (printWriter != null) {
                    printWriter.close();
                }
                try {
                    if (bufferedReader != null) {
                        bufferedReader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    if (socket != null) {
                        socket.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
