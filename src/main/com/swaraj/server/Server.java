package com.swaraj.server;

import java.io.*;
import java.net.*;

/**
 * Created with IntelliJ IDEA.
 * User: swaraj
 * Date: 3/10/13
 * Time: 9:25 PM
 */
public class Server {
    private static int serverCapacity = 20;
    private static ServerSocket serverSocket = null;

    public static void main(String[] args){
        System.out.println("starting the server");
        SocketAddress socketAddress = new InetSocketAddress("localhost",8080);
        try {
            serverSocket = new ServerSocket();
            serverSocket.bind(socketAddress);
            int count = 0;
            while(true){
                if(count<serverCapacity){
                    ServerThread serverThread = new ServerThread();
                    serverThread.setServerSocket(serverSocket);
                    serverThread.start();
                    count++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        serverSocket.close();
    }
}
